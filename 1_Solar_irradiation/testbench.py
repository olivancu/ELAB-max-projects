__author__ = 'Olivier Van Cutsem'

import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt
from building_irradiance import *
from enum import Enum

# --- both window and PV variables ---
LATITUDE = 46.5197 # latitude of Lausanne

# --- window variables ---
WALL_TILT = 90 # in degrees
GLASS_AZIMUTH = 90 # degree of orientation 0 = north, 90 = east, 180 = south, 270 = west
WINDOW_SIZE = 1  # in m2
WINDOW_KL = 0.03 #exctinction coeff. times glazing thickness (most common is glazing thickness of 20mmm
WINDOW_NG = 1.53 # refractive index (main glass are between 1.52 and 1.54 ...)
GLAZING = 2 #1 for single glazing and 2 for double // impact only on the transmittance and impact on output is almost negligeable
class blinds(Enum): #Exterior venetian blinds with three positions
    OPEN = 1
    DIRECT_BLOCK = 2 #Direct radiation blocked
    CLOSED = 3
DIRECT_BLOCK_TRANSM = 0.07 #Radiation transmission coefficient when the blinds block direct sunlight
BLINDS_POSITION = blinds.OPEN #Current position of blinds

# --- PV variables ---
LONGITUDE = 6.6323 # longitude of Lausanne
PV_TILT = LATITUDE #ideally latitude+15 degrees in winter and latitude-15 degrees in summer (future project of gaining up to 4% more perceived radiation by tailoring the tilt angle)
SURFACE_AZIMUTH = 180 # degree of orientation 0 = north, 90 = east, 180 = south, 270 = west

# --- Variable for computing optimal tilt --- FUTURE PROJECT
GROUND_REFL = 0.2 # ground reflectance (assumed to be 0.2 in the paper of the function optimal_fixed_tilt)
T_A_D = 1 # transmittance-absorptance product of the diffuse radiation stream with a value of less than 1 (assumed to be 1 in the paper of the function optimal_fixed_tilt)
T_A_R = 1 # transmittance-absorptance product of the reflected radiation stream with a value of less than 1 (assumed to be 1 in the paper of the function optimal_fixed_tilt)

###
###### Simulation parameters and data
###

dt = 10 * 60  # Time step in seconds
t_start_simu = '2014-07-01'  # Simulation starting time
t_end_simu = '2014-07-21'  # Simulation ending time

try:  # Try to load the dataFrame if it has already been produced
    data_simu = pickle.load(open("var.pickle", "rb"))
except (OSError, IOError) as e:  # Otherwise, read the file, process the data and store the dataFrame
    data_frame = pd.read_csv('swiss-env-data.txt',
                             parse_dates=[0],
                             sep=';',
                             low_memory=False,
                             usecols=['time', 'ods000z0', 'gor000za', 'gre000z0'])

    mask = (data_frame['time'] > t_start_simu) & (data_frame['time'] <= t_end_simu)
    data_simu = data_frame.loc[mask]
    data_simu.columns = ['time', 'ray-diffuse', 'ray-global-std', 'ray-global']
    data_simu.set_index('time', inplace=True)

    # The data set contains "-" values, meaning that the previous value must be repeated
    data_simu = data_simu.replace('-', np.nan)
    data_simu = data_simu.ffill()

    pickle.dump(data_simu, open("var.pickle", "wb"))

###
###### Main loop
###

# This dictionary contains the timeseries data corresponding to the data_frame index dates for the following data:
#  - 'ray-global': the global irradiance (W/m2) read by the weather station
#  - 'ray-diffuse': the diffuse irradiance (W/m2) read by the weather station
#  - 'building-effect': the effect of incoming irradiance on the building, in Watt
#  - 'pv-panel-effect': the incoming irradiance (W/m2) sensed by the PV panel

simulation_data_results = {'ray-global': [], 'ray-diffuse': [],
                           'building-effect': [],
                           'pv-panel-irr': []}

"""Project to compute the optimal tilt
clearness_tot = 0
i = 0
for index, row in data_simu.iterrows():  # Iterate over timeseries data
    # Copy input data as float
    current_ray_global = float(row['ray-global'])
    current_ray_diffuse = float(row['ray-diffuse'])

    # Get clearness index
    times = index.hour*100+index.minute
    equation_of_time = equation_of_time_spencer71(index.dayofyear)
    hour_angle = hour_angle_fn(times, LONGITUDE, equation_of_time)
    declination = declination_cooper69(index.dayofyear)
    solar_zenith = solar_zenith_analytical(LATITUDE, hour_angle, declination)
    clearness_tot = clearness_tot + clearness_fn(solar_zenith, (current_ray_global, current_ray_diffuse))
    i = i + 1
    
clearness = clearness_tot / (i + 1)
tilt = optimal_fixed_tilt(LATITUDE, SURFACE_AZIMUTH, clearness, T_A_D, T_A_R, GROUND_REFL)
"""

for index, row in data_simu.iterrows():  # Iterate over timeseries data
    # Copy input data as float
    current_ray_global = float(row['ray-global'])
    simulation_data_results['ray-global'].append(current_ray_global)
    #print(row['ray-diffuse'])
    #print(index)
    current_ray_diffuse = float(row['ray-diffuse'])
    simulation_data_results['ray-diffuse'].append(current_ray_diffuse)

    # Compute the thermal effect of incoming irradiance on the building inside room
    building_effect = building_thermal_effect(index, (current_ray_global, current_ray_diffuse), LATITUDE, LONGITUDE, WINDOW_SIZE, WINDOW_KL, WINDOW_NG, GLASS_AZIMUTH, WALL_TILT, GLAZING, blinds, BLINDS_POSITION, DIRECT_BLOCK_TRANSM)
    simulation_data_results['building-effect'].append(building_effect)

    # Compute the thermal effect of incoming irradiance on the building inside room
    pv_panel_irr = tilted_surface_irradiance(index, (current_ray_global, current_ray_diffuse), PV_TILT, LATITUDE, LONGITUDE, SURFACE_AZIMUTH)
    #, times, equation_of_time, hour_angle, declination
    simulation_data_results['pv-panel-irr'].append(pv_panel_irr)

###
###### Display results
###

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(data_simu.index.tolist(), simulation_data_results['ray-global'], color='r', label='ray-global')
ax.plot(data_simu.index.tolist(), simulation_data_results['ray-diffuse'], color='k', label='ray-diffuse')

ax.plot(data_simu.index.tolist(), simulation_data_results['building-effect'], color='#FFa500', label='building-effect')
ax.plot(data_simu.index.tolist(), simulation_data_results['pv-panel-irr'], color='g', label='pv-panel-irr')


ax.legend()
plt.grid()
plt.show()
